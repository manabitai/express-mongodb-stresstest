### Summary

This repository is made to stresstest how many requests/s a express.js app can handle
while also updating the database on each request.

An critical part of the performance, is that we queue the mongodb queries and handle them in bulk at an interval.

From the tests (see below), it seems that on average, express.js is able to handle around 10-13K requests per second.

If this number should be increased, then consider using a different library (restify should be faster),
or switch to a more performant language (such as go or rust).

### Requirements

-   [vegeta](https://github.com/tsenart/vegeta) - for running endpoint tests
-   nodejs
-   mongodb connection - to see how much mongodb can impact performance

### Tests

```bash
bash test.sh | tee results.log
```

This runs vegeta for 3 seconds at 1000,3000,6000,10000,50000 requests per second

The output should look something like:

```
Requests      [total, rate, throughput]         3000, 1000.02, 999.97
Duration      [total, attack, wait]             3s, 3s, 147.662µs
Latencies     [min, mean, 50, 90, 95, 99, max]  61.45µs, 431.919µs, 141.744µs, 252.657µs, 416.486µs, 8.348ms, 28.278ms
Bytes In      [total, mean]                     0, 0.00
Bytes Out     [total, mean]                     0, 0.00
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:3000
Error Set:
Requests      [total, rate, throughput]         9000, 3001.03, 3000.88
Duration      [total, attack, wait]             2.999s, 2.999s, 148.701µs
Latencies     [min, mean, 50, 90, 95, 99, max]  51.64µs, 3.882ms, 205.729µs, 475.369µs, 15.896ms, 113.465ms, 136.787ms
Bytes In      [total, mean]                     0, 0.00
Bytes Out     [total, mean]                     0, 0.00
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:9000
Error Set:
Requests      [total, rate, throughput]         17998, 6000.21, 6000.05
Duration      [total, attack, wait]             3s, 3s, 81µs
Latencies     [min, mean, 50, 90, 95, 99, max]  44.051µs, 613.347µs, 237.247µs, 573.501µs, 780.848µs, 14.116ms, 25.844ms
Bytes In      [total, mean]                     0, 0.00
Bytes Out     [total, mean]                     0, 0.00
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:17998
Error Set:
Requests      [total, rate, throughput]         30000, 10000.37, 10000.04
Duration      [total, attack, wait]             3s, 3s, 98.831µs
Latencies     [min, mean, 50, 90, 95, 99, max]  41.05µs, 919.244µs, 274.025µs, 570.869µs, 1.867ms, 21.011ms, 27.257ms
Bytes In      [total, mean]                     0, 0.00
Bytes Out     [total, mean]                     0, 0.00
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:30000
Error Set:
Requests      [total, rate, throughput]         51034, 16961.12, 12791.18
Duration      [total, attack, wait]             3.99s, 3.009s, 980.898ms
Latencies     [min, mean, 50, 90, 95, 99, max]  415.174µs, 1.039s, 823.136ms, 2.495s, 3.149s, 3.295s, 3.455s
Bytes In      [total, mean]                     0, 0.00
Bytes Out     [total, mean]                     0, 0.00
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:51034
Error Set:
```
