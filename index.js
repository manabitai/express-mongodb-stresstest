import "dotenv/config";
import express from "express";
import { MongoClient } from "mongodb";

const app = express();
const client = new MongoClient(process.env.DATABASE_URL);

app.use(express.json());

let queue = [];

app.post("/", async (_, res) => {
    queue.push({
        filter: { _id: "a" },
        update: { $push: { roles: "123" } },
    });

    res.status(200).send();
});

setInterval(async () => {
    const updates = structuredClone(queue);
    queue = [];

    if (!updates.length) return;

    console.log("Batch updating", updates.length);

    await client
        .db()
        .collection("test")
        .bulkWrite(
            updates.map(({ filter, update }) => ({
                updateOne: { filter, update, upsert: true },
            }))
        );
}, 1000);

app.listen(3000);
