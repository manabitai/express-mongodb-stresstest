#!/bin/bash

function run {
    echo "POST http://127.0.0.1:3000" | vegeta attack -name=$1 -duration=3s -rate=$2 > results.$1.bin
    cat results.$1.bin | vegeta report
}

run 1000qps 1000
run 3000qps 3000
run 6000qps 6000
run 10000qps 10000
run 50000qps 50000

vegeta plot results.1000qps.bin results.3000qps.bin results.6000qps.bin results.10000qps.bin results.50000qps.bin > plot.html
